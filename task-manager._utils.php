<?php

    function getCurrentUserHomeDirectory() {
        $userInfo = posix_getpwuid(posix_getuid());
        return $userInfo['dir'];
    }

    function removeFileNameExtension(string $filename) {
        $pathInfoArr = pathinfo($filename);
        return $pathInfoArr['filename'];
    }

    function trimArrayValues(array $array): array {
        $trimmedArray = [];

        foreach ($array as $value) {
            $trimmedArray[] = trim($value);
        }

        return $trimmedArray;
    }

    function checkDirectoryExists(string $dir, bool $create = true) {

        if (file_exists($dir)) {
            if (!is_dir($dir)) { $dir = false;}
        } else {
            if( $create ) {
                if (!mkdir($dir, 0755, true)) {
                    // Directory creation failed
                    $dir = false;
                }
            } else { $dir = false; }
        }

        return $dir;
    }

    function checkFileExists() {
        
    }