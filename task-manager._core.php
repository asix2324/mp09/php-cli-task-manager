<?php

    function getHelpMessage($scriptName) {
        $helpMsg = <<<EOF
        Usage: php $scriptName <action> [options]
        Available actions:
        %s  - Save a new task
                  Example: php $scriptName save 'Task Title' 'Task Content'
        %s  - Store a task, overwriting existing content
                  Example: php $scriptName store 'Task Title' 'Task Content'
        %s  - Delete a task
                  Example: php $scriptName delete 'Task Title'
        %s  - List all tasks
                  Example: php $scriptName list\n
        EOF;

        return sprintf($helpMsg, ACTION_1, ACTION_2, ACTION_3, ACTION_4);

        /*return "Usage: php $scriptName <action> [options]\n"
             . "Available actions:\n"
             . "  ".ACTION_1."   - Save a new task\n"
             . "          Example: php $scriptName save 'Task Title' 'Task Content'\n"
             . "  ".ACTION_2."  - Store a task, overwriting existing content\n"
             . "          Example: php $scriptName store 'Task Title' 'Task Content'\n"
             . "  ".ACTION_3." - Delete a task\n"
             . "          Example: php $scriptName delete 'Task Title'\n"
             . "  ".ACTION_4."   - List all tasks\n"
             . "          Example: php $scriptName list\n";*/
    }

    function isCLI() {
        return (php_sapi_name() === 'cli');
    }

    function isValidAction($action) {
        $avActsArr = getAvailableActions();
        return (!empty($action) && array_key_exists($action, $avActsArr) );
    }

    function isNumArgumentsValid($numArgs) {
        return ($numArgs >= getMinimumArguments() && $numArgs <= getMaximumArguments());
    }

    function getMaximumArguments() {

        $avActsArr      = getAvailableActions();
        foreach ($avActsArr as $action) {
            if ( isset($action['num_arguments']) && ( !isset( $numArguments ) || $numArguments < $action['num_arguments'] ) ) {
                    $numArguments = $action['num_arguments'];
                }
            }
        return $numArguments;
    }

    function getMinimumArguments() {

        $avActsArr      = getAvailableActions();
        foreach ($avActsArr as $action) {
            if ( isset($action['num_arguments']) && ( !isset( $numArguments ) || $numArguments > $action['num_arguments'] ) ) {
                    $numArguments = $action['num_arguments'];
                }
            }
        return $numArguments;
    }

    function getAvailableActions() {
        return array(
            ACTION_1 => array(
                'action'        => ACTION_1,
                'num_arguments' => ACTION_1_NUM_ARGS
            ),
            ACTION_2 => array(
                'action'        => ACTION_2,
                'num_arguments' => ACTION_2_NUM_ARGS
            ),
            ACTION_3 => array(
                'action'        => ACTION_3,
                'num_arguments' => ACTION_3_NUM_ARGS
            ),
            ACTION_4 => array(
                'action'        => ACTION_4,
                'num_arguments' => ACTION_4_NUM_ARGS
            )
        );
    }