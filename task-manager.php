<?php

/*
*   php8.1 task-manager.php save   'Task Title' "1. Bolets\n2.Fruita\netc."
*   php8.1 task-manager.php update 'TaskId' 'Task Title' 'Task Content'
*   php8.1 task-manager.php delete 'TaskId' | 'Task Title'
*   php8.1 task-manager.php list
*/

include_once( 'task-manager._utils.php' );
include_once( 'task-manager._core.php' );
include_once( 'task-manager._csv.php' );

/*
*    composer require symfony/yaml
*
*    require 'vendor/autoload.php';
*
*    use Symfony\Component\Yaml\Yaml;
*
*    $enMssgs = yaml_parse_file( 'messages.en.yaml' );
*/

// Check if script is executed from CLI
if ( isCLI() != true) {
    echo "This script can only be run from the command line interface (CLI).\n";
    return true;
}

const ACTION_1              = 'save';
const ACTION_2              = 'update';
const ACTION_3              = 'delete';
const ACTION_4              = 'list';

const ACTION_1_NUM_ARGS     = 2 +2;
const ACTION_2_NUM_ARGS     = 3 +2;
const ACTION_3_NUM_ARGS     = 1 +2;
const ACTION_4_NUM_ARGS     = 0 +2;

const CSV_DB_NAME           = 'tasks.csv';


if (!isNumArgumentsValid($argc) || !isValidAction($argv[1])) {
    echo getHelpMessage($argv[0]);
    return true;
}

$home       = getCurrentUserHomeDirectory();
$appName    = removeFileNameExtension($argv[0]);
$workDir    = sprintf('%s%2$s%3$s%2$s%4$s', $home, DIRECTORY_SEPARATOR, '.config', $appName );

//Check or Create
if ( empty( checkDirectoryExists($workDir, true) ) ) {
    echo sprintf( "Create %s to proceed.\n", $workDir );
    return true;
}

// Define full CSV database file path
$csvFilePath = sprintf('%s%s%s', $workDir, DIRECTORY_SEPARATOR, CSV_DB_NAME);

//Extract action
$action = strtolower($argv[1]);

// Perform actions based on user input
switch ($action) {
    case ACTION_1:
        if ($argc < 4) {
            return "Usage: php $argv[0] save <title> <content>\n";
        }
        return saveTask($csvFilePath, $argv[2], implode(' ', array_slice($argv, 3)));

    case ACTION_2:
        if ($argc < 4) {
            return "Usage: php $argv[0] store <title> <content>\n";
        }
        return storeTask($csvFilePath, $argv[2], implode(' ', array_slice($argv, 3)));

    case ACTION_3:
        if ($argc < 3) {
            return "Usage: php $argv[0] delete <title>\n";
        }
        return deleteTask($csvFilePath, $argv[2]);

    case ACTION_4:
        return listTasks($csvFilePath);

    default:
        return getHelpMessage($argv[0]);
}
